# Spring Boot Demo

This is sample Spring Boot API which demonstrates the following:

* Spring components using @RestController, @Service and @Repository
* Use of Spring profiles
* Sample implementation of @Value
* Controller and Service layer testing 
* Authentication using Keycloak

## API Testing

This demo contains a sample Controller and Service tests using JUnit, MockMvc and Mockito. 

In the `ControllerTest`, MockMvc is used to call the API endpoints and perform the assertions. MockBean is used to simulate the service object.

* GET `/employees` with result
* GET `/employees` with no result
* GET `/employees/{id}` is found 
* POST `/employees` is successful

In `EmployeeServiceTest`, MockBean is used to simulate the repository.


* `getEmployeesHasResult()`


## Authentication using Keycloak

### Installation and set up of Keycloak Server

Download the Keycloak standalone server - https://www.keycloak.org/downloads.html

Extract the downloaded zip file and navigate to `/keycloak-root/standalone/configuration/standalone.xml`. Update the http port from `8080` to something else like `9000` so that spring-boot-demo API can still use port `8080`.

```
FROM
<socket-binding name="http" port="${jboss.http.port:8080}"/>

TO
<socket-binding name="http" port="${jboss.http.port:9000}"/>
```

Run `./standalone.bat` or `./standalone.sh` to run the Keycloak server. Navigate to http://localhost:9000 and log in with admin account. First time log in will prompt admin user creation.

Once logged in, add a new realm and call it `demo`. Next steps are to create a Client, Role and User.

Click Clients --> Create. Name the client `spring-boot-demo` and click Save. On the next screen (Settings), enter the values below.

```
Client ID: spring-boot-demo
Client Protocol: openid-connect
Access Type: confidential
Valid Redirect URIs: http://localhost:8080/*

# expand the Authentication Flow Overrides
Browser Flow: direct grant
Direct Grant Flow: direct grant
```

Click the Credentials tab and get the Secret value.

Click Roles --> Add Role. Enter `user` for Role Name. Click Save.

Click Users --> Add User. Enter `demouser` for Username. Click Save. On the next screen, click Credentials tab, set `password` for password and turn off Temporary toggle. Click Role Mappings, add `user` to Assigned Roles.

The Keycloak client is now set up and its configuration can be accessed via `http://127.0.0.1:9000/auth/realms/demo/.well-known/openid-configuration`. 

Run the configuration URL above using a GET request. This will return a response containing the Token Endpoint that we will use to generate a a JWT token. Response will look like:

```
{
    "issuer": "http://127.0.0.1:9000/auth/realms/demo",
    "authorization_endpoint": "http://127.0.0.1:9000/auth/realms/demo/protocol/openid-connect/auth",
    "token_endpoint": "http://127.0.0.1:9000/auth/realms/demo/protocol/openid-connect/token",
    ...
}
```

Get the `token_endpoint` and either use curl or Postman to send a POST request to this URL with the following encoded form body:

```
curl -L -X POST 'http://localhost:9000/auth/realms/demo/protocol/openid-connect/token' \
-H 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'client_id=spring-boot-demo' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_secret=bcb96074-ee7f-45b5-b43d-0b89c6ad9885' \
--data-urlencode 'scope=openid' \
--data-urlencode 'username=demouser' \
--data-urlencode 'password=password'
```

This will give a response body containing the `access_token` that will be used in the `Authorization` header property of the spring-boot-demo API.

```
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI3c3N1ZzZNTFY2eUhUV3BLZlhNZWpYTklUem9DZVlWb3dLbUYyZUlFRkRnIn0.eyJleHAiOjE2MDMwNTQ4NjIsImlhdCI6MTYwMzA1NDU2MiwianRpIjoiMjRkOGRmOGUtOTBhZS00Zjg3LTkwMzktZGFkNjczMjE4NjU2IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo5MDAwL2F1dGgvcmVhbG1zL2RlbW8iLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiZDRmNjc4YTctOTJlNi00M2YyLWIyNDAtM2IxNzExYTVlYjM4IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoic3ByaW5nLWJvb3QtZGVtbyIsInNlc3Npb25fc3RhdGUiOiIwZDdlMjgyMC0wZjFjLTQxZGEtOGU1NC1mOGY3YjEyNTYyZTMiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJkZW1vdXNlciJ9.TBrLILb3JNRM_Yhz3unXIUnEusZ1UMKychy7QIB2IqAzKC-1vklNW3fKHDwXso6omERShJgsyiPALXd2UMrpo54JWGr4-eIMpBhLJJHt7WGSV1znhP4XCPUx_ZtZ55foeuC6Cb-0i9ZeInCozqCs-oD8Hulcok09WOzZK13fayYp3JN8yxOFfwYH2xlp2E-zwm0cj8Mu6UT-lvc3lFuU3Zhccz9OlYdoHHG6XsZtpfb2XUPM6AmSGFz-QXeysEzytlucJdYprpiXthwvBhm0pRMhLJ9vO01pi3lbS9FYCLD_wLWbGe-IBVjmPYR48esQreDxYLvVZ6jjawLh5OBOWw",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIyNGNkMGVlMy1jNDk1LTQxMzktYTJkZS0xMzY5OGQ1Njg3NzgifQ.eyJleHAiOjE2MDMwNTYzNjIsImlhdCI6MTYwMzA1NDU2MiwianRpIjoiMmUwNTA3MGEtNTFiZi00NTYwLTkxN2ItYWQ5ZmNjYTk3NWVlIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo5MDAwL2F1dGgvcmVhbG1zL2RlbW8iLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjkwMDAvYXV0aC9yZWFsbXMvZGVtbyIsInN1YiI6ImQ0ZjY3OGE3LTkyZTYtNDNmMi1iMjQwLTNiMTcxMWE1ZWIzOCIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJzcHJpbmctYm9vdC1kZW1vIiwic2Vzc2lvbl9zdGF0ZSI6IjBkN2UyODIwLTBmMWMtNDFkYS04ZTU0LWY4ZjdiMTI1NjJlMyIsInNjb3BlIjoiZW1haWwgcHJvZmlsZSJ9.StHfud88tOtiA1fXX5yszDcGBhor70QjSp4eRBHbK2w",
    "token_type": "bearer",
    "not-before-policy": 0,
    "session_state": "0d7e2820-0f1c-41da-8e54-f8f7b12562e3",
    "scope": "email profile"
}
```

### Testing using spring-boot-demo API

In the `application.properties`, make sure the following Keycloak configurations are present. The values should match with the Keycloak setup:

```
keycloak.auth-server-url=http://localhost:9000/auth
keycloak.realm=demo													# keycloak realm
keycloak.resource=spring-boot-demo									# keycloak client ID
keycloak.public-client=true
keycloak.security-constraints[0].authRoles[0]=user			# this is the keycloak role and not `demouser` user
keycloak.security-constraints[0].securityCollections[0].patterns=/*, /employees/*
```

In Postman, add a GET `http://localhost:8080/employees` request and add `Authorization` header property with a value `Bearer <paste access_token value here>`. 


### References

* https://www.keycloak.org/2017/05/easily-secure-your-spring-boot.html
* https://developers.redhat.com/blog/2020/01/29/api-login-and-jwt-token-generation-using-keycloak/

