package com.example.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Employee;
import com.example.service.EmployeeService;

@RestController
public class Controller {
	
	@Autowired
	EmployeeService service;
	
	@Value("${message:hello default}")
	private String message;
	
	@GetMapping("/message")
	public ResponseEntity getMessage() {
		return ResponseEntity.ok().body(message);
	}
	
	@RequestMapping(path = "/message2", method = RequestMethod.GET, produces = "text/plain")
	public ResponseEntity getMessageUsingResponseMapping() {
		return ResponseEntity.ok().body(message);
	}
	
	//==================================

	
	@GetMapping("/employees")
	public ResponseEntity getEmployees() {
		return ResponseEntity.ok().body(service.getEmployees()); 
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity getEmployeeById(@PathVariable("id") Integer id) {
		return ResponseEntity.ok().body(service.getEmployeeById(id)); 
	}
	
	@PostMapping("/employees")
	public ResponseEntity saveEmployee(@RequestBody Employee employee) {
		Employee createdEmployee = service.saveEmployee(employee);
		try {
			return ResponseEntity
					.created(new URI("/employees"))
					.body(createdEmployee);
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error in POST /employees");
		} 
	}
}
