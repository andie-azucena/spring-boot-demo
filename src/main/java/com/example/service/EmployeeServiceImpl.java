package com.example.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository repo;
	
	public EmployeeServiceImpl() { }
	
	public EmployeeServiceImpl(EmployeeRepository repo) {
		this.repo = repo;
	}
	
//    public void setRepository(EmployeeRepository repo) {
//        this.repo = repo;
//    }
	
	@Override
	public List<Employee> getEmployees() {
		return repo.getEmployees();
	}
	
	@Override
	public Optional<Employee> getEmployeeById(Integer id) {
		return repo.getEmployeeById(id);
	}
	
	@Override
	public Employee saveEmployee(Employee employee) {
		return repo.saveEmployee(employee);
	}

}
