package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.model.Employee;

public interface EmployeeService {

	List<Employee> getEmployees();
	
	Optional<Employee> getEmployeeById(Integer id);
	
	Employee saveEmployee(Employee employee);
}
