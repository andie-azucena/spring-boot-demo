package com.example.repository;

import java.util.List;
import java.util.Optional;

import com.example.model.Employee;

public interface EmployeeRepository {

	List<Employee> getEmployees();
	
	Optional<Employee> getEmployeeById(Integer id);
	
	Employee saveEmployee(Employee employee);
}
