package com.example.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.example.model.Employee;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

	private List<Employee> employees = new ArrayList<Employee>();
	
	public EmployeeRepositoryImpl() {
		// mocked data; pretend it came from DB
		Employee emp1 = new Employee(1, "Devorah", "Manager", 5);
		Employee emp2 = new Employee(2, "Leil", "Director", 10);
		Employee emp3 = new Employee(3, "Rogelio", "Supervisor", 3);
		
		employees.addAll(Arrays.asList(emp1, emp2, emp3));
	}
	
	@Override
	public List<Employee> getEmployees() {	
		return employees;
	}
	
	@Override
	public Optional<Employee> getEmployeeById(Integer id) {
		return employees.stream().filter(emp -> emp.getId() == id).findAny();
	}
	
	@Override
	public Employee saveEmployee(Employee employee) {
		employee.setId(employees.size() + 1);
		employees.add(employee);
		return employee;
	}
}
