package com.example;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.example.model.Employee;
import com.example.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

	@Autowired
    private MockMvc mockMvc;
    
	@MockBean
	private EmployeeService service;
	
	@Test
	@DisplayName("GET /employees WITH RESULT")
	void getEmployeeListHasResult() throws Exception {
		// Mocked the employee and the service
		Employee emp1 = new Employee(1, "John", "Manager", 10);
		Employee emp2 = new Employee(2, "Jane", "Director", 15);
		Employee emp3 = new Employee(3, "James", "Supervisor", 5);
		
		List<Employee> list = new ArrayList<Employee>();
		list.add(emp1);
		list.add(emp2);
		list.add(emp3);
		
		doReturn(list).when(service).getEmployees();
		
		// Execute the request
		mockMvc.perform(get("/employees"))
		
			// Validate the response code and content type
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		
			// Validate the response body
			.andExpect(jsonPath("$.[0].id", is(1)))
			.andExpect(jsonPath("$.[0].name", is("John")))
			.andExpect(jsonPath("$.[0].position", is("Manager")))
			.andExpect(jsonPath("$.[0].years", is(10)));
	}
	
	@Test
	@DisplayName("GET /employees NO RESULT")
	void getEmployeeListNoResult() throws Exception {
		// Mocked the employee and the service
		doReturn(new ArrayList<Employee>()).when(service).getEmployees();
		
		// Execute the request
		mockMvc.perform(get("/employees"))
		
			// Validate the response
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))		
			.andExpect(content().string("[]"));
	}
	
	@Test
	@DisplayName("GET /employees/2 is FOUND")
	void getEmployeeByIdFound() throws Exception {
		// Mocked the employee and the service
		Employee employee = new Employee(2, "Jane", "Director", 15);
		doReturn(Optional.of(employee)).when(service).getEmployeeById(2);
		
		// Execute the request
		mockMvc.perform(get("/employees/{id}", 2))
		
			// Validate the response code and content type
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		
			// Validate the response body
			.andExpect(jsonPath("$.id", is(2)))
			.andExpect(jsonPath("$.name", is("Jane")))
			.andExpect(jsonPath("$.position", is("Director")))
			.andExpect(jsonPath("$.years", is(15)));
	}
	
	@Test
	@DisplayName("POST /employees is SUCCESSFUL")
	void addEmployeeSuccess() throws Exception {
		// Set up new employee to add and mock profile to return 
		Employee mockEmp = new Employee(1, "John", "Manager", 10);
		doReturn(mockEmp).when(service).saveEmployee(mockEmp);
		
		// Execute the request
		mockMvc.perform(post("/employees")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(mockEmp)))
		
			// Validate the response
			.andExpect(status().isCreated())
			.andExpect(header().string(HttpHeaders.LOCATION, "/employees"));
	}
	
	
	private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
