package com.example;

import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Employee;
import com.example.repository.EmployeeRepository;
import com.example.service.EmployeeService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EmployeeServiceTest {

	@Autowired
	EmployeeService service;
	
	@MockBean
	EmployeeRepository repo;
	
	@Test
	@DisplayName("TEST getEmployeesHasResult")
	void getEmployeesHasResult() throws Exception {
		// Mocked the employee and the repo
		Employee emp1 = new Employee(1, "John", "Manager", 10);
		Employee emp2 = new Employee(2, "Jane", "Director", 15);
		Employee emp3 = new Employee(3, "James", "Supervisor", 5);
		List<Employee> list = new ArrayList<Employee>();
		list.add(emp1);
		list.add(emp2);
		list.add(emp3);
		doReturn(list).when(repo).getEmployees();
		
		// Call service
		List<Employee> returnedList = (List<Employee>) service.getEmployees();
		
		// Validate
		Assertions.assertFalse(returnedList.isEmpty(), "No result.");
		Assertions.assertSame(returnedList.get(0), emp1, "Employee should be the same.");
		Assertions.assertEquals(returnedList.get(2).getName(), "James");
	}
}
